package com.snowruin.poi;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {
	
	private static   ExcelUtil eu=new ExcelUtil();
	
	private ExcelUtil(){}
	
	public static ExcelUtil getInstance(){
		return eu;
	}
	
	private ExcelTemplate handlerObj2Excel(Map<String, String> map,String template,List objs,Class clazz,boolean isClassPath) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		
		ExcelTemplate et=ExcelTemplate.getInstance();
		
		if(isClassPath){
			et.readExcelTemplateByClasspath(template);
		}else{
			et.readExcelTemplateByPath(template);
		}
		
		List<ExcelHeader> headerList = this.getHeaderList(clazz);
		
		//排序
		Collections.sort(headerList);
		
		//写入标题
		et.createNewRow();
		for (ExcelHeader excelHeader : headerList) {
			System.out.println(excelHeader);
			et.createCell(excelHeader.getTitle());
		}
		
		//写入数据
		for (Object obj : objs) {
			et.createNewRow();
			for (ExcelHeader excelHeader : headerList) {
				//获得方法名称
				/*Method method = clazz.getDeclaredMethod(mn);
				Object returnValue = method.invoke(obj);
				et.createCell(returnValue.toString());*/
				
				et.createCell( BeanUtils.getProperty(obj, getMethodName(excelHeader)));
				
			}
		}
		et.replaceFinalData(map);
		return et;
	}
	
	private String getMethodName(ExcelHeader excelHeader){
		String mn=excelHeader.getMethodName().substring(3);
		
		mn=mn.substring(0,1).toLowerCase()+mn.substring(1);
		
	//	System.out.println(mn);
		
		return mn;
	}
	
	/**
	 * 将对象输出到流
	 * @param map
	 * @param template
	 * @param os
	 * @param objs
	 * @param clazz
	 * @param isClassPath
	 */
	public void exportObj2ExcelByTemplate(Map<String, String> map,String template,OutputStream os,List objs,Class clazz,boolean isClassPath) {
		try {
			ExcelTemplate et=this.handlerObj2Excel(map, template, objs, clazz, isClassPath);
			
			et.writeToStream(os);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 通过模板 导出对象数据到Excel文件
	 * @param map  常量
 	 * @param template  模板
 	 * @param outPutPath  制定输出Excel的目录
	 * @param objs  数据
	 * @param clazz 对象（javaBean）
	 * @param isClassPath  是否通过classpath来读这个模板
	 * 
	 */
	public void exportObj2ExcelByTemplate(Map<String, String> map,String template,String outPutPath,List objs,Class clazz,boolean isClassPath) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		
		try {
			ExcelTemplate et=this.handlerObj2Excel(map, template, objs, clazz, isClassPath);
			
			et.writeToFile(outPutPath);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private Workbook handlerObj2Excel(List objs,Class clazz, boolean isXssf){
		Workbook wk=null;
		try {
			if(isXssf){
				wk=new XSSFWorkbook();
			}else{
				wk=new HSSFWorkbook();
			}
			
			Sheet sheet=wk.createSheet();
			
			Row r= sheet.createRow(0);
			
			//获取头
			List<ExcelHeader> headers=this.getHeaderList(clazz);
			Collections.sort(headers);  //排序
			
			//写标题
			for (int i=0;i<headers.size();i++) {
				r.createCell(i).setCellValue(headers.get(i).getTitle());
			}
			
			Object obj=null;
			//写数据
			for (int i = 0; i < objs.size(); i++) {
				r=sheet.createRow(i+1);
				
				obj=objs.get(i);
				
				for (int j = 0; j < headers.size(); j++) {
					r.createCell(j).setCellValue(BeanUtils.getProperty(obj, this.getMethodName(headers.get(j))));
				}
				
				
			}
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return wk;

	}
	
	/**
	 * 基于没有模板的导出Excel
	 * @param outPath
	 * @param objs
	 * @param clazz
	 * @param     isXssf
	 * @throws IOException 
	 */
	public void exportObj2Excel(String outPath,List objs,Class clazz,boolean isXssf) throws IOException{
			Workbook wb=handlerObj2Excel(objs, clazz, isXssf);
			
			FileOutputStream fos=null;
			try {
				fos = new FileOutputStream(new File(outPath));
				wb.write(fos);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}finally{
				if(fos!=null){
					fos.close();
				}
			}
	}
	
	public void exportObj2Excel(OutputStream os,List objs,Class clazz,boolean isXssf){
		Workbook wb = this.handlerObj2Excel(objs, clazz, isXssf);
		try {
			wb.write(os);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 获取headerList
	 * @param clazz
	 * @return
	 */
	public List<ExcelHeader> getHeaderList(Class clazz){
		List<ExcelHeader> headers=new ArrayList<ExcelHeader>();	

		List<String> annotationNames = new ArrayList<String>();

       //获取 clazz 的所有方法
		Method[] methods = clazz.getDeclaredMethods();

		// 获取clazz 的 所有 成员变量
		Field[] fields = clazz.getDeclaredFields();

		for (Method method : methods) {
			String mn=method.getName();
			//判断是否是get开头的方法
			if(mn.startsWith("get")){
				//判断 此方法上是否有ExcelResource类的注解
				if(method.isAnnotationPresent(ExcelResource.class)){
					//得到该注解对象
					ExcelResource excelResource = method.getAnnotation(ExcelResource.class);
				    System.out.println(excelResource.title()+" "+excelResource.order());
				    
				    headers.add(new ExcelHeader(excelResource.title(),excelResource.order(),mn));

					annotationNames.add(mn);
				}
			}
		}

		for (Field field : fields){
			String fieldName = field.getName();
			fieldName = fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1);
			if(field.isAnnotationPresent(ExcelResource.class)){
				if(!annotationNames.contains("get"+fieldName)){
					ExcelResource excelResource = field.getAnnotation(ExcelResource.class);
					headers.add(new ExcelHeader(excelResource.title(),excelResource.order(),"get"+fieldName));
				}
			}
		}

		return headers;
	}
	
	/**
	 * 通过Excel文件读取 列标题所对应的方法名称
	 * @return
	 */
	private Map<Integer, String> getHeaderMaps(Row titleRow,Class clazz){
		
		Map<Integer,  String > map=new HashMap<Integer, String>();
		List<ExcelHeader> headerList = this.getHeaderList(clazz);

		Collections.sort(headerList);
		
		for (Cell cell : titleRow) {
			String title=cell.getStringCellValue();
			
			for (ExcelHeader excelHeader : headerList) {
				if(excelHeader.getTitle().equals(title.trim())){
					map.put(cell.getColumnIndex(), excelHeader.getMethodName().replace("get", "set"));
					break;
				}
			}
		}
		
		return map;
	}
	
	public <T> List<T>  readExcel2ObjsByClassPath(String path,Class<T> clazz){
		return this.readExcel2ObjsByClassPath(path, clazz, 0,0);
	}
	
	public <T> List<T>  readExcel2ObjsByPath(String path,Class<T> clazz){
		return this.readExcel2ObjsByPath(path, clazz, 0,0);
	}
	
	
	public <T> List<T> readExcel2ObjsByClassPath(String path,Class<T> clazz,int readLine,int tailLine){
		Workbook wb=null;
		try {
			wb=WorkbookFactory.create(ExcelUtil.class.getResourceAsStream(path));
			return handlerExcel2Objs(wb, clazz, readLine,tailLine);
			
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	 public <T> List<T> readExcel2ObjsByInputstream(InputStream inputStream , Class<T> clazz){
		return readExcel2ObjsByInputstream(inputStream,clazz,0,0);
	 }

	public <T> List<T> readExcel2ObjsByInputstream(InputStream inputStream , Class<T> clazz, int readLine, int tailLine ){

		Workbook workbook = null;
		try {
			workbook = WorkbookFactory.create(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		return handlerExcel2Objs(workbook,clazz,readLine,tailLine);
	}
	
	public <T> List<T> readExcel2ObjsByPath(String path, Class<T> clazz, int readLine, int tailLine){
		Workbook wb=null;
		try {
			wb=WorkbookFactory.create(new File(path));
			return handlerExcel2Objs(wb, clazz, readLine,tailLine);
			
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public <T> List<T> handlerExcel2Objs(Workbook wb,Class<T> clazz,int readLine,int tailLine){
		Sheet sheet = wb.getSheetAt(0);
		
		Row row = sheet.getRow(readLine);
		
		Map<Integer, String> maps = this.getHeaderMaps(row, clazz);
		if(maps==null || maps.size()<=0){
			throw new RuntimeException("要读取的Excel文件格式不正确，请检查是否设定了合适的行");
		}
		
		List<T> datas =new ArrayList<T>();
		
		T t ;
		try {
			for (int i = readLine+1; i <= sheet.getLastRowNum()-tailLine; i++) {
				row=sheet.getRow(i);
				t = clazz.newInstance();

				for (Cell cell : row) {
					int ci = cell.getColumnIndex();
					
					String mn=  maps.get(ci).substring(3);
					mn=mn.substring(0,1).toLowerCase()+mn.substring(1);
					
					BeanUtils.copyProperty(t, mn, this.getValue(cell));
				}
				datas.add(t);
			}
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return datas;
	}
	
	 /**
     * 得到Excel表中的值
     * 
     * @param cell
     *            Excel中的每一个格子
     * @return Excel中每一个格子中的值
     */
    @SuppressWarnings("static-access")
	private String getValue(Cell cell) {
        if (cell.getCellType() == cell.CELL_TYPE_BOOLEAN) {
            // 返回布尔类型的值
            return String.valueOf(cell.getBooleanCellValue());
        } else if (cell.getCellType() == cell.CELL_TYPE_NUMERIC) {
            // 返回数值类型的值
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return String.valueOf(cell.getStringCellValue());
        } else {
            // 返回字符串类型的值
            return String.valueOf(cell.getStringCellValue());
        }
    }
 
}
