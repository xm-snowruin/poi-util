package com.snowruin.poi;

/**
 * Excel 头部
 * @author zhaoxinmin
 *
 */
public class ExcelHeader implements Comparable<ExcelHeader>{
	
	
	/**
	 * 列标题
	 */
	private String title;
	
	/**
	 * 排序
	 */
	private int order;
	
	/**
	 * 方法名称
	 */
	private String methodName;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	

	public ExcelHeader(String title, int order, String methodName) {
		this.title = title;
		this.order = order;
		this.methodName = methodName;
	}
	
	@Override
	public String toString() {
		return "ExcelHeader [title=" + title + ", order=" + order
				+ ", methodName=" + methodName + "]";
	}

	/**
	 * 比较  0 表示相等
	 */
	public int compareTo(ExcelHeader o) {
		return order>o.order?1:(order<o.order?-1:0);
	}
	
	

}
