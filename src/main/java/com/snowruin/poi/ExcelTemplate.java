package com.snowruin.poi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * Excel 模板
 * @author zhaoxinmin
 * @ 2016-08-26
 */
public class ExcelTemplate {
	private final static String DATA_LINE="datas";
	
	private final static String DEFAULT_STYLE="defaultStyle";
	
	private final static String STYLE="styles";
	
	private final static String SER_NUM="sernums";
	
	private static ExcelTemplate et=new ExcelTemplate();
	
	private Workbook wb;
	
	private Sheet sheet;
	/**
	 * 初始化列的下标
	 */
	private int initColIndex;

	/**
	 * 初始化列的下标
	 */
	private int initRowIndex;
	
	/**
	 * 当前列的下标
	 */
	private int currColIndex;
	
	/**
	 * 当前行的下标
	 */
	private int currRowIndex;
	
	/**
	 * 当前行
	 */
	private Row curRow;
	
	/**
	 * 最后一行的下标
	 */
	private int rowLastIndex;
	
	/**
	 * 默认样式
	 */
	private CellStyle defaultStyle;
	
	/**
	 * 行高
	 */
	private float rowHeight;

	/**
	 * 存储某一行所对应的样式  Integer： 列号     CellStyle：样式
	 */
	private Map<Integer, CellStyle> styles=null;
	
	/**
	 * 序号所在的列下标
	 */
	private int serColIndex;
	
	/**
	 * 序号所在的行下标
	 */
	/*private int serRowIndex;*/
	
	private ExcelTemplate(){
		

	}
	
	public static ExcelTemplate getInstance(){
		return et;
	}
	
	// 1、读取相应的文档
	public ExcelTemplate readExcelTemplateByClasspath(String path){
			
			try {
				wb=WorkbookFactory.create(ExcelTemplate.class.getResourceAsStream(path));
				
				initTemplate();
			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException("读取的模板格式有错！请检查......");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new RuntimeException("读取的模板文件不存在！请检查......");
			}
		return this;
	}
	
	public ExcelTemplate readExcelTemplateByPath(String path){
		
		try {
			wb=WorkbookFactory.create(new File(path));
			
			initTemplate();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("读取的模板格式有错！请检查......");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("读取的模板文件不存在！请检查......");
		}
		
		return this;
	}
	
	/**
	 * 初始化模板
	 */
	private void initTemplate(){
		//获取sheet
		sheet=wb.getSheetAt(0);
		
		initConfigData();
		
		rowLastIndex=sheet.getLastRowNum();
		
		//createNewRow();
		curRow=sheet.createRow(currRowIndex);
	}
	
	public void createCell(String value){
		Cell c=  curRow.createCell(currColIndex);
		
		setCellStyle(c);
		
		c.setCellValue(value);
		
		currColIndex++;
	}
	
	public void createCell(int value){
		Cell c=  curRow.createCell(currColIndex);
		
		setCellStyle(c);
		
		c.setCellValue((int)value);
		
		currColIndex++;
	}
	
	public void createCell(double value){
		Cell c=  curRow.createCell(currColIndex);
		
		setCellStyle(c);
		
		c.setCellValue(value);
		
		currColIndex++;
	}
	
	public void createCell(boolean value){
		Cell c=  curRow.createCell(currColIndex);
		
		setCellStyle(c);
		
		c.setCellValue(value);
		
		currColIndex++;
	}
	
	public void createCell(Date value){
		Cell c=  curRow.createCell(currColIndex);
		
		setCellStyle(c);
		
		c.setCellValue(value);
		
		currColIndex++;
	}
	
	public void createCell(Calendar value){
		Cell c=  curRow.createCell(currColIndex);
		
		setCellStyle(c);
		
		c.setCellValue(value);
		
		currColIndex++;
	}
	
	public void setCellStyle(Cell c){
		if(styles.containsKey(currColIndex)){
			c.setCellStyle(styles.get(currColIndex));
		}else{
			c.setCellStyle(defaultStyle);
		}
	}
	
	public void createNewRow(){
		//向下移动行
		if(rowLastIndex>currRowIndex && currRowIndex!=initRowIndex){
			sheet.shiftRows(currRowIndex, rowLastIndex, 1, true,true);
			rowLastIndex++;
		}
		curRow=sheet.createRow(currRowIndex);
		currRowIndex++;
		curRow.setHeightInPoints(rowHeight);
		
		currColIndex=initColIndex;
	}
	
	/**
	 * 初始化配置
	 */
	@SuppressWarnings("static-access")
	private void initConfigData(){
		boolean findData=false;
		boolean findSer=false;
		
		for (Row row:sheet) {
			if(findData){
				break;
			}
			for (Cell c:row) {
				//判断 datas是否为String类型
				if(c.getCellType()!=c.CELL_TYPE_STRING){
					continue;
				}
				
				String str = c.getStringCellValue().trim();
				
				if(str.equals(SER_NUM)){
					serColIndex=c.getColumnIndex();
					//serRowIndex=row.getRowNum();
					findSer=true;
				}
				
				if(str.equals(DATA_LINE)){
					initColIndex=c.getColumnIndex();
					initRowIndex=row.getRowNum();
					
					currColIndex=initColIndex;
					currRowIndex =initRowIndex;
					
					findData=true;
					defaultStyle=c.getCellStyle();
					rowHeight=row.getHeightInPoints();
					initStyles();
					break;
					
				}
			}
			
			
		}
		if(!findSer){
			initSer();
		}
		System.out.println(currRowIndex+" , "+currColIndex);
		
	}
	
	/**
	 * 初始化序号
	 */
	private void initSer() {
		for (Row row:sheet) {
			for (Cell c:row) {
				//判断 datas是否为String类型
				if(c.getCellType()!=c.CELL_TYPE_STRING){
					continue;
				}
				
				String str = c.getStringCellValue().trim();
				
				if(str.equals(SER_NUM)){
					serColIndex=c.getColumnIndex();
					//serRowIndex=row.getRowNum();
				}
			}
			
			
		}
	}

	/**
	 * 初始化样式
	 */
	private void initStyles() {
		styles=new HashMap<Integer, CellStyle>();
		for (Row row:sheet) {
			for (Cell c:row) {
				//判断 datas是否为String类型
				if(c.getCellType()!=c.CELL_TYPE_STRING){
					continue;
				}
				
				String str = c.getStringCellValue().trim();
				
				if(str.equals(DEFAULT_STYLE)){
					defaultStyle=c.getCellStyle();
				}
				
				if(str.equals(STYLE)){
					styles.put(c.getColumnIndex(),c.getCellStyle());
				}
			}
			
			
		}
	}

	/**
	 * 将数据写入文件  并 生成Excel 文件
	 * @param filePath
	 */
	public void writeToFile(String filePath){
		FileOutputStream fos=null;
		try {
			fos=new FileOutputStream(filePath);
			
			wb.write(fos);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("写入的文件不存在："+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("写入的数据失败："+e.getMessage());
			
		}finally{
			if(fos!=null){
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
	}
	
	/**
	 * 数据写入流中
	 * @param os
	 */
	public void writeToStream(OutputStream os){
		 try {
			wb.write(os); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("写入流失败："+e.getMessage());
		}
	
	}
	
	/**
	 * 根据map替换一些常量
	 * @param datas
	 */
	public void replaceFinalData(Map<String, String> datas){
		if(datas==null || datas.size()==0){
			return;
		}
		for (Row row:sheet) {
			for (Cell c:row) {
				//判断 datas是否为String类型
				if(c.getCellType()!=c.CELL_TYPE_STRING){
					continue;
				}
				
				String str = c.getStringCellValue().trim();
				
				if(str.startsWith("#")){
					if(datas.containsKey(str.substring(1))){
						c.setCellValue(datas.get(str.substring(1)));
					}
				}
			 }
		 }
	
	 }
	
	/**
	 * 插入序号
	 */
	public void insertSer(){
		int index=1;
		Row row=null;
		Cell c=null;
		for (int i = initRowIndex; i < currRowIndex; i++) {
			row=sheet.getRow(i);
			c=row.createCell(serColIndex);
			setCellStyle(c);
			c.setCellValue(index++);
		}
	}
	
}