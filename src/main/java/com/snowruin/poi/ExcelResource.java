package com.snowruin.poi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注解  用来描述excel
 * @author zhaoxinmin
 * @date 2016-08-27
 */
//在运行时运行
@Retention(RetentionPolicy.RUNTIME)
@Target(value =  {ElementType.FIELD , ElementType.METHOD})
public @interface ExcelResource {

	/**
	 * title
	 * @return
	 */
	String title();
	
	
	/**
	 * 排序
	 * @return
	 */
	int order()  default 9999;
	
	
}
