package com.snowruin.poi.model;

import com.snowruin.poi.ExcelResource;

public class User1 {

	@ExcelResource(title="用户标识",order=1)
	private int id;

	@ExcelResource(title="用户名",order=2)
	private String username;

	@ExcelResource(title="用户昵称",order=3)
	private String nickname;

	@ExcelResource(title="年龄",order=4)
	private int age;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}



	public User1() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User1(int id, String username, String nickname, int age) {
		super();
		this.id = id;
		this.username = username;
		this.nickname = nickname;
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", nickname="
				+ nickname + ", age=" + age + "]";
	}
	
	
	
}
