package com.snowruin.poi.test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.snowruin.poi.model.User1;
import org.apache.commons.beanutils.BeanUtils;
import org.junit.Test;

import com.snowruin.poi.ExcelTemplate;
import com.snowruin.poi.ExcelUtil;
import com.snowruin.poi.model.User;

public class TestPoi {

	public void test01(){
		ExcelTemplate et = ExcelTemplate.getInstance().readExcelTemplateByClasspath("/excel/default.xls");
		
		et.createNewRow();
		
		et.createCell(111);
		et.createCell("张三");
		et.createCell("张先生");
		et.createCell("werw324324");
		
		et.createNewRow();
		
		et.createCell(222.333);
		et.createCell("张三1");
		et.createCell("张先生1");
		et.createCell("werw3243241");
		
		et.createNewRow();
		
		et.createCell(new java.util.Date());
		et.createCell("张三2");
		et.createCell("张先生2");
		et.createCell("werw324324");
		
		et.createNewRow();
		
		et.createCell("44444444444");
		et.createCell("张三3");
		et.createCell("张先生3");
		et.createCell("werw324324");
		
		et.createNewRow();
		
		et.createCell("555555555555");
		et.createCell("张三4");
		et.createCell("张先生4");
		et.createCell("werw324324");
		
		et.createNewRow();
		
		et.createCell(true);
		Calendar c=Calendar.getInstance();
		c.setTime(new Date());
		et.createCell(c);
		et.createCell("张先生5");
		et.createCell("werw324324");
		
		//et.createNewRow();
		
		Map<String, String> map=new HashMap<String, String>();
		map.put("title", "招生简章");
		map.put("date", "2016-12-12 12:12");
		map.put("dep","招生处");
		
		et.replaceFinalData(map);
		
		et.insertSer();
		
		et.writeToFile("d:/test/poi/test01.xls");
	}
	
	public void testBeanUtils(){
		try {
			User user=new User();
			BeanUtils.copyProperty(user, "username", "张三");
			String username = BeanUtils.getProperty(user, "username");
			System.out.println(username);
			
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testObj2Xls() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, IOException{
		List<User> list=new ArrayList<User>();
		
		list.add(new User(0,"张是哪", "张三", 12));
		list.add(new User(1,"张是1", "张三1", 121));
		list.add(new User(2,"张是2", "张三2", 122));
		
		list.add(new User(3,"张是3", "张三3", 123));
		list.add(new User(4,"张是4", "张三4", 124));
		list.add(new User(5,"张是5", "张三5", 125));
		
		Map<String, String> map=new HashMap<String, String>();
		map.put("title","招生就业");
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		
		map.put("date", sdf.format(new Date()));
		
		//基于模板的导出
		//ExcelUtil.getInstance().exportObj2ExcelByTemplate(null, "/excel/user.xls", "d:/test/poi/user.xls", list, User.class, true);
		
		ExcelUtil.getInstance().exportObj2Excel("d:/test/poi/obj2Excel.xlsx", list, User.class, true);
	}

	@Test
	public void testRead(){
		List<User> list = ExcelUtil.getInstance().readExcel2ObjsByPath("d:/test/poi/obj2Excel.xlsx", User.class,0,0);
		for (User u : list) {
			System.out.println(u);
		}
	}


	@Test
	public void testRead1(){
		List<User1> list = ExcelUtil.getInstance().readExcel2ObjsByPath("d:/test/poi/obj2Excel.xlsx", User1.class,0,0);
		for (User1 u : list) {
			System.out.println(u);
		}
	}
	
	
	
}
